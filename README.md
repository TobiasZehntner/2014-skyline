# Skyline (2014)

Artwork by [Tobias Zehntner](http://www.tobiaszehntner.art)

- Year: 2014
- License: [APGL-3.0](https://www.gnu.org/licenses/agpl.html)
- [Documentation](http://www.tobiaszehntner.art/work/skyline)

### Description
LED strip controlled through Arduino and indirectly openFrameworks

This is the code for my work Skyline (2014). An LED strip controlled by Arduino, with values calculated by openFrameworks. The Arduino cycles through keyframes, putting out the color to the LED strip by fading between keyframes. Check my [website](http://www.tobiaszehntner.art) for details.

#### openFrameworks

The code for openFrameworks reads out the RGB values of each pixel from a keyframe (vertically), and writes it as output, including information on how many pixels and how many keyframes there are. Copy/paste the readout to Arduino. Place keyframes named as <####.jpg> in the `bin` folder of your openFrameworks app directory.

#### Arduino
The code in the Arduino file takes the RGB values of the keyframes, calculates the color between them as time passes, and sends them to each RGB LED pixel on the strip.
